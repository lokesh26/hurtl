class ApplicationMailer < ActionMailer::Base
  default from: "noreply@sh.com"
  layout 'mailer'
end
