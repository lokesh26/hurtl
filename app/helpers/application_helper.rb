module ApplicationHelper
  def full_title title
    if not title.empty?
      return title
    else
      return "Forgot title"
    end
  end
end
